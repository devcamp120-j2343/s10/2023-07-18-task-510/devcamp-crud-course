//B1: khai báo thư viện mongoose
const mongoose = require('mongoose')

//B2: khai báo class Schema
const Schema = mongoose.Schema

//B3: khởi tạo Schema với các thuộc tính của collection
const reviewSchema = new Schema({
    _id : mongoose.Types.ObjectId,
    stars: {
        type:Number,
        default: 0
    },
    note: {
        type:String,
        required:false
    }
});

//B4: Biên dịch schema thành model
module.exports = mongoose.model("review", reviewSchema)